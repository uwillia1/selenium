package com.selenium.webdriver.basic;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.JavascriptExecutor;

public class Day1 {
	
	private WebDriver driver;
	private JavascriptExecutor jse;

	public Day1() {
		this.jse = (JavascriptExecutor)driver;
	}
	
	public void invokeBrowser() {
		try {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\will\\Documents\\Selenium\\chromedriver.exe");
			this.driver = new ChromeDriver();
			this.driver.manage().deleteAllCookies();
			this.driver.manage().window().maximize();
			this.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			this.driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			this.driver.get("http://www.youtube.com/");
			this.searchVideo("Lacuna");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void searchVideo(String searchTerms) {
		if (searchTerms == null) {
			throw new IllegalArgumentException("Search terms cannot be null");
		}
		driver.findElement(By.id("search")).sendKeys(searchTerms);
		driver.findElement(By.id("search-icon-legacy")).click();
		driver.findElement(By.id("dismissable")).click();
		
	}

	public static void main(String[] args) {
		Day1 day1 = new Day1();
		day1.invokeBrowser();
	}

}
